

# Running existing workflows


Let's now run our first workflow in production.
In this section, we'll cover the case where a workflow has already been assembled and we  want to execute it with real datasets.


As an example, we will use a simple pre-built workflow called "NLP Demo 1" (for Neuro-Linguistic Programming).

![](media_running/10.jpg)


Here is a step-by-step description of this workflow:
- Trigger an execution by passing as input a Tweet (text) in a user form
- Transform the input text to a format accepted by the next module
- Take the Tweet and use an off-the-shelf DistilBERT model (fine-tuned for text classification) to evaluate the sentiment in those segments of text.


*For a more advanced and automated version of this workflow, please check the [next section](./assembling).*




### Deploying workflows


As you learned from the "Ryax concepts" section, within Ryax, a workflow has to
be in the "deployed" state to properly run
in production. Deployment is the phase where the workflow is built,
packaged and loaded by Ryax on production infrastructures.


Before deploying a workflow, we'll have to make a quick sanity check to ensure it is ready to do so.
Fortunately, Ryax provides such an insight through
the workflows' statuses.


Let's move in the Ryax Studio and consider the following workflow: "NLP Demo 1".

![](media_running/11.jpg)


This workflow is already assembled and, as you can see, displays an "OK" status.
This is a good sign.

As a reminder of workflow statuses:


<table style="width: 100%; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_running/350901b6-2b3b-4734-8a08-b9bdf2451461.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
<p><span style="text-decoration: underline;">OK</span><span style="  ">: workflow is properly assembled and configured, it is ready to be deployed.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_running/f40a4c47-c443-4ed1-a2ca-832541c5e890.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
<p><span style="text-decoration: underline;">Incomplete</span><span style="  ">: workflow is not ready to be deployed (configuration is incomplete, assembly is incorrect, etc).</span></p>
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_running/fedc39f6-8963-4b4c-b22d-58f9cc79f861.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
<p><span style=" text-decoration: underline;">Creating</span><span style="  ">: workflow deployment is initiated and the workflow is being instantiated in production.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_running/ce104518-6679-42c9-bbfa-422c3ff32238.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
<p><span style="text-decoration: underline;">Deploying</span><span style="  ">: workflow is being deployed.</span></p>
</div></td>
</tr>
<tr class="odd">
<td><div style="position: relative; padding: 13.248px;">

![](media_running/8c2fadbc-dfc6-4fd4-a1b6-171dd9ccb27c.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
<p><span style="text-decoration: underline;">DeployError</span><span style="  ">: workflow could not be deployed. Check module configuration for warnings or errors.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div style="position: relative; padding: 13.248px;">

![](media_running/26c7b050-ad40-457e-8ada-62015df95410.png)

</div></td>
<td><div style="position: relative; padding: 13.248px;">
<p><span style="text-decoration: underline;">Deployed</span><span style="  ">: workflow is ready to receive inputs and generate executions.</span></p>
</div></td>
</tr>
</tbody>
</table>


To complete our sanity check, let's move in the Studio Editor by clicking on our workflow's row in the list.

![](media_running/2318896b-7d36-47ef-b09b-57600fd3765e.png)

Once in the Editor, you can click on the "Warning" (!) panel (on the bar to the right) to open it:

![](media_running/ec59f7ab-a055-4b7e-b220-39ee628572fd.png)

This panel displays any warning or errors Ryax has detected on your workflow.
If the "Workflow ready" message displays, in accordance to the "OK" status of the workflow, then we're ready to proceed with the deployment.

Additionally, you can also check that all modules display a green check on their top right corner, meaning that they are individually properly configured and interlinked:

<span
style=" width: 200px; height: 89.6825px; display: inline-block;">![](media_running/a2adf7b8-06aa-42aa-92dd-681b161136f9.png)</span>

You can now click any "Deploy" button, either from the Workflow list view or from the Studio Editor:

<span
style=" width: 100px; height: 45.9716px; display: inline-block;">![](media_running/70083153-f0fd-4085-9309-ed52ca1e3d3c.png)</span>

Upon deployment, your workflow will display a succession of statuses:
"Creating", "Deploying", and finally "Deployed".
It should take a few seconds.
If the displayed workflow status does not change, please click the "Refresh" button at the top of the page to actualize the status.

The "Deployed" status ensures that your workflow has been successfully deployed in production: congratulations!


### Executing workflows

Now that your workflow is deployed, it is ready to receive inputs/triggers and generate executions.

As a reminder, within Ryax an "execution" means one run of a module, i.e. one module doing its job (computing, getting data, ...) once.
For more information on executions, please refer to the [Ryax concepts](../concepts/concepts).


Depending
on how your workflow is built, the type of events leading to workflow
triggering may differ. In the case of the current workflow, the
triggering event is the submitting of a form, as indicated by the first
module (i.e. "source" module) in our workflow:


<span
style=" width: 199.78px; height: 97.1659px; display: inline-block;">![](media_running/12.jpg)</span>


Once we
deployed our workflow, this first module has been automatically
instantiated and is now running, but this module in particular awaits a
user action to generate an execution.

``` note:: Ryax workflows can be triggered by a wide variety of 'events', such as timers, user inputs, file watchers, sensor data streams, API calls, etc.
```

To access the triggering form, click on the "Tweet input form" module, the Setup pane will open.
From the "Properties" tab, click the "Integrated form" button:

<span
style=" width: 200px; height: 310.537px; display: inline-block;">![](media_running/b7737184-ad0b-4a78-851e-5b8bbb6905f8.png)</span>


You can then enter any Tweet text and click "submit" to trigger your workflow.
Here are Tweet examples for you to test (please enter them separately, with one form submission per Tweet):

> *I think that Twitter is a great way to share my thoughts and opinions.*

> *Twitter may be the way people cry their hearts out to the world instead of keeping it to themselves.*

> *What happened of pocketbooks and poet diaries when we have Twitter now?*

![](media_running/1e2770dc-978e-43a8-89ee-e7758eb1441f.png)

Upon submitting, a confirmation message will display, and the successive
executions of each module in the workflow will display in a list at the
bottom:

![](media_running/3d1ad4d8-28fc-4d45-96dd-8dabd27151f1.png)

You can also access the triggering form from the Studio list, by clicking on the Execution button to the right of the workflow's row:

![](media_running/13.jpg)

You can
repeat this process several times with the supplied examples or with
your own Tweets, each "Submit" generating a single execution for each
module in the workflow from end to end.

Now that
your workflow has been deployed and triggered in production, let's
observe its executions and at last(!) its results.


### Assessing workflows’ executions

Now that
our workflow is deployed and we have understood how and when it is
triggered, let's have a look at the executions it generates.
We'll
follow the natural downstream direction of the workflow, step by step
until we reach the final results it generates.

To have a
look at your workflow runs, you can click the "View executions" button
at the top of the Studio screen, or click the "Executions" part of the
menu to the left:

<span
style="width: 500px; display: inline-block;">![](media_running/b6e1daaa-6194-4954-85a6-30cd980e62cc.png)</span>

You'll
then be redirected to this workflow's execution list:

![](media_running/d14a3c4e-0ee1-456a-a783-f7afa6724eae.png)

With the list, you'll be able to access each module's execution details.
Let's click on any of the "Form" execution in the list:


#### Form executions

As covered in the [Ryax tour](../tutorials/handling), the Execution dashboard provides you with a lot of information about this specific module's run:
when it started, how long it lasted, its current status,
its timeline in the context of the whole workflow, its inputs and outputs, etc.

<span
style=" width: 600px; display: inline-block;">![](media_running/bcfc587b-05fb-49e8-96bf-a8e8e7ff844e.png)</span>

For a simple form source module like this one, the two interesting things we
can observe are:
- The "outputs" section: displays the actual Tweet we've submitted to the form.
- The "next executions" section: shows the next execution as per the workflow propagation. By clicking on it, you'll be able to jump  right to it directly from the current execution dashboard.


#### Transform module executions

Let's have a look at the next module's execution, either from the execution list or from the previous module quick link:

<span
style=" width: 600px; display: inline-block;">![](media_running/023c0f59-bd02-465b-ba67-ae500cf6a6e3.png)</span>

Among other things, what you can note from this module's execution is:
- This module actually prints logs, which you can read directly from Ryax for debug purposes or to ensure that all went well.
- Same as before, you can get a quick access to upstream/downstream executions directly from the previous/next panel at the bottom of the dashboard.


#### Text Classifier executions


Let's move to our last - and most interesting - module:
the processor for text classification.


You will
see that the log of this module is much substantial, and provides one
way to access the workflow's result (see the red rectangle at the
top):

![](media_running/d1912ad7-fd76-4b1f-b792-4f18cc0de3cc.png)

You can
read the result of the classification (positive or negative), as well as
the confidence index. Any confidence index above 90-95% can be
considered a very strong prediction.

For example, in the case of the Tweet "What
happened of pocketbooks and poet diaries when we have Twitter
now?", the
classification is "NEGATIVE" and the
confidence index is very high at "99.93573427200317%".

You can also get a file with the text classification result from the "output" panel:

<span
style=" width: 500px; display: inline-block;">![](media_running/621db91a-a7da-4039-9b6e-dad5dd2409ea.png)</span>

This is a very basic way to access the results of a workflow.
In the next example of this guide, we will show you how this can be done in a more industrial way, using the fancier "Publisher" modules.
