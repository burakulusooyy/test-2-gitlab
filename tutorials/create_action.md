# Create your first trigger and processor actions

This is a quick start guide to Ryax. Welcome, I hope you enjoy using Ryax as much
as we enjoin developing it. This tutorial requires some basic python3 knowledge. You can access all code from this tutorial in the git repository [https://gitlab.com/ryax-tech/training/tuto-create-action](https://gitlab.com/ryax-tech/training/tuto-create-action). That's all, let's jump in.


## Getting started

Throughput out this tutorial we are going to build a ryax workflow from scratch. This toy application 
is a simple json reader that compute the mean values for several parameters. To make this workflow
interesting we are going to define 2 actions: 

* `json-trigger` : trigger action that will execute a single time injecting a pre-defined json as input.
* `analyze-json` : processor action that receives a json file as input and compute the mean for every list of values;


## How to create a python3 Ryax Processor Action?

Ryax Processor Actions are the processing
blocks of a Ryax Workflow, so often a workflow have many actions of this type. 
To define a Ryax Action we first need to create a directory structure
to hold some files, then we can choose a logo, create its specification, and finally write some code.
As any python application you can use a `requirements.txt` file to define 
dependencies to external libraries.
When you believe your action is ready we still highly recommended you to test it locally using
a python virtual environment.

  
### Ryax Action directory structure

A typical Ryax Action directory structure follows the convention:

```shell
- my-action-name
  - logo.png            # optional logo in png format, must be a 250x250 pixels image
  - requirements.txt    # list of python external dependencies, equivalent of pip dependencies file
  - ryax_metadata.yaml  # action definition
  - ryax_handler.py     # action python3 code
```

### Write Ryax Action specifications

A Ryax Action accomplishes anything that requires some processing.
An action goes as close to a function as possible, that is, we have a set of inputs and then return
a set of outputs based on some useful computation. For this example we will make an action that
will receive several list of values in a json formatted file. Compute the mean for each value, and finally
output this a new json file with the mean for every value. An example input would be:

```json
{
  "speed": [30, 55, 60, 70],
  "accelaration": [3, 4, 6, 7]
}
```

For this input the output would be like below. That is, for the `2` given parameter on the example `speed` and 
`accelaration` we will have single float number that has the mean across all values.

```json
{
  "speed": 53.75,
  "accelaration": 5
}
```

The content of `ryax_metadata.yaml` will define several aspects of the action: its name, a detailed description,
a version, type, the kind (processor, publisher, or trigger). Very important, `ryax_metadata.yaml` has also the 
definition  of the action's input/output, each is a list of all the parameters an action is supposed to receive as 
input and a detailed list of parameters the action return as output. For each input/output, we have a name, a more
UI friendly name, a help that can show example values, and finally a type. So the a possible `ryax_metadata.yaml`
file for our action would be like below.

```yaml
apiVersion: "ryax.tech/v2.0"
kind: Processor
spec:
  id: analyze-json
  human_name: Compute mean for each json parameter.
  type: python3
  version: "1.0.7"
  logo: "logo.png"
  description: "Analyze json file computing the mean for each key parameter."
  categories:
  - Processor
  - JSON
  inputs:
  - help: Json file with a list of values per key.
    human_name: json_in
    name: json_in
    type: file
  outputs:
  - help: Json file with a list of values to return as output.
    human_name: json_out
    name: json_out
    type: string
```


### Code using python3

This action implement a very simple algorithm we just iterate through the json parameters, and for
each parameters' list we compute the mean. We encapsulate all the action logic in a single function that
must have name `handle`, have an input dict and return a dict as output.

```python
import json


def handle(params: dict) -> dict:
    json_out = {}
    with open(params["json_in"], 'r') as json_f:
        json_data = json.load(json_f)
    for k in json_data:
        print(f"Computing the mean for {k}...")
        sum = 0
        for v in json_data[k]:
            sum += v
        json_out[k] = sum / len(json_data[k])
        print(f"====> {json.dumps(json_out[k])}")

    return {"json_out": json.dumps(json_out)}
```

The python code within the `handle` function is called every time a new execution is triggered. The execution is 
triggered by a previous action. In order to start a workflow, a first action must be enacted from the outside
world, this type of action that bridges an external action is called a trigger action. We will make a trigger action
to test this processor action later. 

### Locally testing

We can now make a simple test for the action by locally running it. For that
to work we have to provide a sample set of valid inputs. A common approach to test an action is to create a main
call that executes the `handle` function. For our example this would be something like below.

```python
# The main is not used by ryax but is a quite easy way of testing the action locally
if __name__ == "__main__":
    print(
        handle({
            "json_in": "in.json",
        })
    )
```

Note that input parameter `json_in` is indeed a file in the local filesystem. 
Within Ryax file parameters are always a full path reference. If you use a relative path, correct the path
of the commands below if necessary. You can tell the problem by checking the the python exception in case of 
errors. To test let us first create a virtual environment to install the dependencies.

```shell
python -m venv .venv
```

Activate the virtual environment.

```shell
source .venv/bin/activate
```

Use pip to install all dependencies on the requirements.

```shell
pip install -r requirements.txt
```

**Note** : some python packages are just wrapping around more complex libraries/programs, you might need
to install these dependencies as well. This might highly depend on your operating system. The more accurate
will be to use nix-shell to create a virtual environment.

Finally, to run locally, first create a file `json.in` with a content that respect the
input format defined earlier and the path defined on the main python function.

```json
{
  "speed": [30, 55, 60, 70],
  "accelaration": [3, 4, 6, 7]
}
```

Finally, run locally using.

```shell
python3 ryax_handler.py
```

The output should be the mean for each dict parameter, like below.

```python
{'json_out': {'speed': 53.75, 'accelaration': 5.0}}
```

## How to create a trigger action?

To start the execution it is required a trigger action. A trigger action creates executions based 
on external events. In this quick start guide we are going to create a very simple action trigger.
One that just loop undefinetly creating executions at every `sleep` seconds. 
Ryax has several examples of triggers: based on cron expressions, create execution upon a rest call, 
make an execution out of a user html form. 

### Specification of Ryax Trigger Action

The trigger action is a special action that has a different filename for the ryax entrypoint. A
typical trigger action have the following files stored in the action's directory.

```yam
json-trigger
  - logo.png
  - requirements.txt
  - ryax_metadata.yaml
  - ryax_run.py
```

Note that the only difference from another type of action is the name of the python file which is `ryax_run.py`
instead of `ryax_handler.py`. The trigger definition is different as well, kind is `Source` and that is all.
An action of type trigger has equally inputs and outputs, that define a respective list of parameters and 
values to return. It is common that a trigger action has an empty list of inputs, even though it is not mandatory 
is very likely that a trigger action have some kind of output. In our case this trigger will have 2 input
parameters a `json_text` string and an integer `sleep_time` to use as the number of seconds to sleep between 
executions.

```yaml
apiVersion: "ryax.tech/v2.0"
kind: Source
spec:
  id: json-trigger
  human_name: Create a very simple json trigger.
  version: "1.0.7"
  logo: "logo.png"
  type: python3
  description: > 
    Trigger a workflow with a list of parameters each with a random set of values.
  categories:
  - Trigger
  inputs:
  - help: Json file with data to trigger the workflow.
    human_name: json_text
    name: json_text
    type: longstring
  - help: Sleep duration in seconds.
    human_name: sleep_time
    name: sleep_time
    type: integer
  outputs:
  - help: Json file with data to trigger the workflow.
    human_name: json_file
    name: json_file
    type: file
```

### Code to trig an execution

The function called name and type change slightly from other types of actions. In this case it is
an `async` function that has 2 parameters, the first `service` is fullfilled by ryax with an 
object that implement the create_run method, this method is used to trigger the execution
and receive as parameter the expected trigger's action outputs.

```python
#!/usr/bin/env python3
import asyncio

async def run(service, input_values: dict) -> None:
    while True:
        print(f"Sleeping for {input_values['sleep_time']}...")
        await asyncio.sleep(input_values['sleep_time'])
        temp_filename = "/tmp/json.out"
        with open(temp_filename, "w") as temp_f:
            temp_f.write(input_values['json_text'])
        print(f"Running execution with file content {input_values['json_text']}...")
        await service.create_run(
            {
                'json_file': temp_filename,
            }
        )
```


### Test locally

Start by creating the virtual environment and install the asyncio dependency. Note that is best not to
reference asyncio on the requirements to avoid conflicts with ryax specific used version.

```shell
python -m venv .venv
source .venv/bin/activate
pip install asyncio
```

Now we can use a simple function to test the action code. To replace the service parameter we 
use `AsyncMock`.

```python
# Used for testing only
if __name__ == "__main__":
    from unittest.mock import AsyncMock
    import asyncio
    asyncio.run(run(AsyncMock(),
        {
            "json_text": """{
                "speed": [30, 55, 60, 70],
                "accelaration": [3, 4, 6, 7]
            }""",
            "sleep_time": 10,
        })
    )
```

Note that any input go on a dictionary as input parameter, in this case we have 2 mandatory inputs:

* `json_text`: string containning a json;
* `sleep_time`: number of seconds to sleep between executions;

The trigger must run in a continuous loop.

## How to build a Ryax Action ?

To create actions ryax will interface with a git repository. The protocol used to clone is http. So, you must
use the https link to the project. If the project requires you might as well use credentials: username and password.
Following we assume that you already have a git repo for your application.

### Build the action on Ryax

Log in on Ryax and go to `module builder`.

![module builder](media_create_action/module_builder.png)

Click on `New Repository`.

![new repository](media_create_action/new_repository.png)


Add the information of the repository where you created the actions, give it a name, 
add the https link to clone the project, fulfill your username and password info and after click in save.

![add repository](media_create_action/filled_repository.png)


A new repository should appear on the list. Now click on `scan repository`. 

![scan repository 1](media_create_action/scan_repository_1.png)

Click on  `new scan` then choose the `git reference`
(the branch, commit short sha, or tag) you want to scan and click on the `scan repository` button of the pop-up window.

![scan repository 2](media_create_action/scan_respository_2.png)

Wait for a list of actions to appear. For every one of the actions that 
appear, click on the lightning icon. This will start building the actions.


![build list](media_create_action/build_list.png)

To monitor actions' build status you can click on `module builds`, this will show a list
of current building actions with their status: `pending`, `building`, `success`. 

![build status 1](media_create_action/build_status_1.png)
![build status 2](media_create_action/build_status_2.png)

Wait for all your actions to reach the green `success` status. Once you have all actions built,
you can inspect the list of available actions and check the actions are there.

![available actions](media_create_action/available_actions.png)

## Creating my first workflow


To start building the application  click dashboard. There you can see an empty list of workflows
so you can create a new one using `create one now` button.


![create workflow](media_create_action/create_workflow.png)

You will start editing a workflow right away. The first thing to do is to select a name to the workflow
by clicking in the edit icon, typing the new name, and than click on the `ok` button.

![workflow edit 1](media_create_action/workflow_edit_name_1.png)
![workflow edit 2](media_create_action/workflow_edit_name_2.png)

Next we must select the trigger action of the workflow. Do that by clicking on source and then
selecting action `json-trigger` from the list of available triggers.

![workflow edit 3](media_create_action/workflow_edit_add_trigger.png)

Now configure the trigger input parameters, we will use `10` as the default sleep_time and ``
as `json_text`.

![workflow edit 4](media_create_action/workflow_edit_configure_trigger_1.png)
![workflow edit 5](media_create_action/workflow_edit_configure_trigger_2.png)

Add the `analyze-json` action next.

![workflow edit 6](media_create_action/workflow_edit_add_processor.png)

Finally link the parameter from `analyze-json` to the output of the trigger.

![workflow edit 7](media_create_action/workflow_edit_link_processor_input_1.png)
![workflow edit 8](media_create_action/workflow_edit_link_processor_input_2.png)


And this is it, your workflow is now ready to be deployed.

### Deploying the workflow

To execute the workflow code we need to first deploy it. To deploy the workflow click on the `deploy` button.

![workflow deploy](media_create_action/workflow_deploy.png)

Once the workflow is deployed you gonna see an undeploy button insteand, you might need to refresh the page.
You can then see the current executions.

![workflow deploying](media_create_action/workflow_deploying.png)
![workflow deployed](media_create_action/workflow_deployed.png)

### Inspecting the execution

To see the executions click on the execution link.

![workflow run 1](media_create_action/workflow_runs.png)
![workflow run 2](media_create_action/workflow_execution_1.png)
![workflow run 3](media_create_action/workflow_execution_2.png)
![workflow run 4](media_create_action/workflow_execution_3.png)


There you can see the different actions and the logs associated to each one and the input/output values that
were given as parameters. Check the `analyze-json` action output, to see if the mean was correctly computed.

![workflow execution logs 1](media_create_action/workflow_execution_logs_1.png)
![workflow execution logs 2](media_create_action/workflow_execution_logs_2.png)

## Final considerations

This workflow is a toy example that will run many executions using the json given as input. This is not
ideal because we must stop and redeploy the workflow every time we need to feed new input. Moreover this workflow
will consume a lot of resources, so undeploy the workflow as soon as possible. To improve from
this workflow you can check the `postgw` a form that trigger executions based on the trigger output parameters.
Form trigger is a default action and can be found clicking [here](https://gitlab.com/ryax-tech/workflows/default-actions/-/tree/master/triggers/postgw).

