# How to add module from gitlab ?

1. First begin by adding a git repository

login onto the webUI and go to the « Repo manager »

![Repository screenshot](../_static/repository-screen/repo-1.png "Repository screenshot")

From there you can either : 
- create a new repository
- scan an existing repository

![Repository screenshot](../_static/repository-screen/repo-2.png "Repository screenshot")

For now we want to add a new repository, so click on the « New repository button »

![Repository screenshot](../_static/repository-screen/repo-3.png "Repository screenshot")

fill out the form and click « save » to add the repository

![Repository screenshot](../_static/repository-screen/repo-4.png "Repository screenshot")

and then, click to scan repository

now we need to fill our gitlab credentials to perform the scan

![Repository screenshot](../_static/repository-screen/repo-5.png "Repository screenshot")

Both the username and password are required, in the last field we can choose a branch on which we would have modules, by default the selected branch is master

![Repository screenshot](../_static/repository-screen/repo-6.png "Repository screenshot")

now that our credentials are entered, let’s click on the « scan repository » button

![Repository screenshot](../_static/repository-screen/repo-7.png "Repository screenshot")

and voila, now we have a list of modules we can build, to do this, click on the « Build module » button 

![Repository screenshot](../_static/repository-screen/repo-8.png "Repository screenshot")

we then got a success notification

2. we now can navigate to the « module builder » page 

![Repository screenshot](../_static/repository-screen/repo-9.png "Repository screenshot")

On this page we can see all the modules

They are sorted by date, the last one being the first in the list

We got a number of informations about each module, from which repository it comes from, it's name, it's build satatus (ready, pending, building, success, error), a short description, the creation date, it's version.

We also got actions, we can : 

- retry the build

![Repository screenshot](../_static/repository-screen/repo-10.png "Repository screenshot")

- delete the modules

![Repository screenshot](../_static/repository-screen/repo-11.png "Repository screenshot")

- see it's documentation

![Repository screenshot](../_static/repository-screen/repo-12.png "Repository screenshot")


what these does : 

- the retry button allow to trigger the build of a module, if this one as failed in the scan list, you can only retry the build if the module status is one of the following : ready or error

- the delete button allow to delete a module, you can only delete a module if it's status is one of the following : pending, ready or error

- the documentation button allow to navigate to the complete documentation of a module, click on it to go to the module store, you can only access it if the module status is success
