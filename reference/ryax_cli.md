# Command Line Interface 

The Ryax Command Line Interface, or Ryax CLI for short, is the swiss army 
knife for all your automation needs. Without going through the UI you can
do CRUD operations on workflows and modules. 

``` warning:: The CLI is in beta. It may change at any time.
```


## Installation

You will need Ryax CLI to unleash the full power of Ryax. 
In most cases, you can install Ryax CLI by using pip3 as below.

```sh
wget https://docs.ryax.tech/_static/ryax-cli/ryax_cli-latest-py3-none-any.whl
pip3 install ryax_cli-latest-py3-none-any.whl
```
## Download

If you are having troubles downloading with wget just download from the link below.
<https://docs.ryax.tech/_static/ryax-cli/ryax_cli-latest-py3-none-any.whl>


## Running

Run Ryax CLI and check the help page.

```sh
ryax-cli
```

If you have a `command not found` error, the installation did not work.


## Options

```eval_rst

+------------------------+---------------------------------------------------------------------------------+
| Option                 | Description                                                                     |
+========================+=================================================================================+
| --help                 | Print help, can be used with specific command.                                  |
+------------------------+---------------------------------------------------------------------------------+
| --server=`url`         | URL to Ryax API, for instance, `--server=https://myryax.tech.org/api`           |
+------------------------+---------------------------------------------------------------------------------+
| --output-format=`json` | Show output in `json` instead of default human readable. Also accepts `yaml`.   |
+------------------------+---------------------------------------------------------------------------------+
| --verify_ssl=`True`    | Default is `True`, setting it to `False` will disable ssl verification, unsafe. |
+------------------------+---------------------------------------------------------------------------------+
| --password=`pass`      | Send `pass` string as password to void prompting, requires `--username`.        |
+------------------------+---------------------------------------------------------------------------------+
| --username=`user`      | Send `user` string as username to void prompting, requires `--password`.        |
+------------------------+---------------------------------------------------------------------------------+
```

## Quick Reference

Commands fit 3 categories: Admin, Modules, and Workflows. We detail each category next.
When output examples are present, 
the issued command line starts with a dollar sign ($) and the output is 
just below the command.

### Admin

Administration commands can be used to login, manage users, or monitor the execution of workflows.

* `login` : prompt for username and password, create a session file to avoid asking to login everytime.
```sh
$ ryax-cli --server=https://myryax.tech.org/api login
```

* `user` : create, list, update, or delete users

  * Create user, will prompt for password
```sh
$ ryax-cli --server=https://myryax.tech.org/api user create username
```

  * List users
```sh
$ ryax-cli --server=https://myryax.tech.org/api user list
|                    id|   user|  role|             email|            comment|
| usr-1601469257-kwvf30| myuser| Admin|   myuser@ryax.org|                   |
| usr-1601449386-cwsl5w|  user1| Admin| contact@ryax.tech| Default admin user|
```

  * Delete user, need to provide user's id
```sh
$ ryax-cli --server=https://myryax.tech.org/api user delete usr-1601449386-cwsl5w
Are you sure you want to delete user usr-1601469257-kwvf30? [n]|y: y
User usr-1601469257-kwvf30 was deleted!
```

## NixOS Users

You might need a virtual environment to install ryax-cli avec pip. A virtual environment assures you can make changes without risking to populate your global python environment. If you are a user of nixos, here are two guides for sandboxing ryax-cli installation with `venv` and `nix-shell`.


* venv

To create a virtual environment, type:

```sh
python3 -m venv ryax-crashcourse
```

Enter the directory to switch to the virtual environment.

```sh
cd ryax-crashcourse
```

* nix-shell

If you like nix you can simulate a python `venv` using `nix-shell`, just
copy the content below to file `shell.nix`.

```nix
with import <nixpkgs> {};
mkShell {
 name = "python-devel";
 venvDir = "venv";
 
 buildInputs = with python37Packages; [ 
  aiohttp
  fire
  pyyaml
  requests
 ];
 postShellHook = ''pip install `curl https://ryax.tech.org/ryax-cli-latest.tgz`''; }
```

To create a simulated `venv` with ryax-cli working, run the command below on the same location where the `shell.nix` file is.

```sh
nix-shell
```
