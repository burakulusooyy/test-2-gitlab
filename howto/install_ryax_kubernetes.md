# Install Ryax on Kubernetes

We assume that you are comfortable with Kubernetes and therefore we do not provide too many details on the Kubernetes parts of the installation.

## Preparatory steps

- Make sure your kubectl point to the intended cluster: `kubectl config current-context`.
- Make sure it is a kubernetes cluster dedicated to ryax: we offer no guarantee that Ryax runs smoothly alongside other applications.
- Make sure you have complete admin access to the cluster. Try to run `kubectl auth can-i create ns` or `kubectl auth can-i create pc`, for instance.

```sh
$ kubectl auth can-i create ns
Warning: resource 'namespaces' is not namespace scoped
yes
```



## Customize your installation

Installing Ryax is analogous to installing a Helm chart. To begin we will start with a default configuration, and make a few tweaks so that everything is compatible with your Kubernetes provider. Be assured however that you will be able to fine-tune your installation later on.

First create a directory to organize the ryax installation.

```bash
mkdir ryax_install
```

Just like helm charts, we call "values" the configuration of a Ryax cluster. Let's initialize a basic values file.



```bash
docker run -v $PWD/ryax_install:/data/volume -u $UID ryaxtech/ryax-adm:latest init --values volume/ryax_values.yaml
```
Edit the file with vim Or your favorite text editor.

```bash
vim ryax_values.yaml
```

The `clusterName` value is the name you give to your cluster, which is used in various places. One of those places is the URL of your cluster that will end up being \<clusterName\>.\<domainName\>.io, therefore it has to be consistent with your DNS. If you do not intend to configure a DNS cluster, just leave this to the default value, in this case beaware you will access ryax through the ip address directly and https certificate will be self-signed.

Choose your version, latest stable version is `22.10.0-beta1`.

The detail of all the values can be found in [ryax-adm/helm-charts/values.yaml](https://gitlab.com/ryax-tech/ryax/ryax-adm/-/blob/master/helm-charts/values.yaml), feel free to make them suit your needs. We recommend that you start small with the volume sizes, as you can't shrink them later on without deleting them, which will force you to backup/restore your data. The default values give comfortable volume sizes to start working on the platform, you can always scale them later.

An example of a simple values file for ryax.

```yaml
───────┬───────────────────────────────────────────────────────────────────────
       │ File: ryax_install/ryax_values.yaml
───────┼───────────────────────────────────────────────────────────────────────
   1   │ clusterName: mybackend
   2   │ datastore:
   3   │   pvcSize: 2Gi
   4   │ domainName: local.ryax.io
   5   │ filestore:
   6   │   pvcSize: 10Gi
   7   │ logLevel: info
   8   │ monitoring:
   9   │   enabled: true
  10   │ registry:
  11   │   pvcSize: 10Gi
  12   │ storageClass: default
  13   │ tls:
  14   │   enabled: true
  15   │ version: '22.10.0-beta1'
───────┴───────────────────────────────────────────────────────────────────────
```

## Install Ryax

Once you have customized your values you may install Ryax on your cluster :

```bash
docker run -v $PWD/ryax_install:/data/volume -v $KUBECONFIG:/data/kubeconfig.yml -u $UID ryaxtech/ryax-adm:latest apply --values volume/ryax_values.yaml
```

If the installation fails, check the logs, check your configuration and try again. If you are lost you join our [Discord server](https://discord.gg/ctgBtx9QwB) and we will be happy to help!

## Configure your DNS

The last step is configuring your DNS so that you can connect to your cluster. The address you should register is \<clusterName\>.\<domainName\>.ryax.io.

To retrieve the external IP of your cluster, run this one-liner
```bash
kubectl -n kube-system get svc traefik -o jsonpath='{.status.loadBalancer.ingress[].ip}'
```

If you want something easier to type day to day: `kubectl -n kube-system get svc traefik`, under "External IP".

