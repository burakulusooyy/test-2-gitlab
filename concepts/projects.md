# Projects

In Ryax, a **project** groups and controls the visibility of workflows, actions, runs, repositories, and users. All resources within a project are only visible to users that are associated with that project.

Projects help you organize your resources allowing you to show only the relevant workflows, actions, and repositories for a group of people. This type of organisation can significantly improve the user experience and can be done in a variety of ways, for example:
- split teams across different projects, so each team has only access to what is relevant to them.
- group all your workflows by theme, client, or any relevant aspect you might think.
- separate test and production-oriented projects so developers and users have more freedom to experiment.
- create a personal project for each user, so they are free to learn and test Ryax.

A user can be associated with several projects, and a user is required to be associated to at least one project. To do so, use the **project** combobox at the top-left.

![Project selection](../_static/concepts/projects_combobox.png "Project selection image")

Administrators can see all projects and assign users to many projects. Be careful, in some situations a user may end up being assigned to no project at all; in this case, the user will be unable to see any resource in Ryax. If this is the case ask an administrator to assign a project.

To create a project, you have to be an administrator. Click on the "Projects" menu, then click on the "New project" button in the top-right.

![Project creation](../_static/concepts/projects_new.png "Project creation image")

To assign users to a project, click on the project name on the same page, and then click on the "Add user" button.

![Project add user](../_static/concepts/projects_useradd.png "Project add user image")

At the moment, Ryax cannot migrate resources (like a workflow) from one project to another. If you need to do that, you can export the resource and import it to the other project.

