# Use cases

Following are a few concrete examples of what Ryax is capable of, but keep in mind this list is non-exhaustive, and does not come close to detailing all the potential use-cases Ryax can cover!

## PoC and MVP

With Ryax users can develop apps quickly. It is well suited to create Proofs-of-Concept (PoC) and Most-Valuable-Products (MVP). Users can leverage a library of existing Actions and Triggers, as well as develop specific ones for their project, giving them a fast-track to a working piece of software.

## Creating API-first backends

Without any knowledge in web server, hosting, or HTTP request handling, users can still create web-based applications with Ryax API-first backends. By leveraging the "API endpoint" trigger, workflows triggered by HTTP endpoints can be created in a few clicks.

## Automate daily tasks

At its core, Ryax is a powerful workflow automation tool. Thus, users can create automation to speed up their daily tasks, whether those be personal, or large scale industrial tasks for larger businesses.

## Data Industrialization

Ryax can be used to operate complex event-driven streaming data flows, commonly used in Industrial IoT or other Manufacturing 4.0 applications. 

Popular use-cases usually involve collecting data from various machine sensors in real-time, cleaning it, filtering it, injecting it into a processing algorithm, and outputting valuable results as quickly as possible.

Predictive workflows are widely used in industrial contexts to anticipate machine failures, production peaks, or plan maintenance. Ryax is well suited for this kind of application.

## Lab Automation

Laboratory Automation is a key topic in various lab-powered industries such as pharmaceutics, healthcare, biotechnologies, cosmetics, etc.

Supported by the right tools and processes, lab automation endeavors can deeply transform businesses and enhance their performance in many operational fields. Ryax is helping laboratory teams collect, clean, and crunch experimental data to extract its full value potential.

Another main use for lab automation is "lab industrialization": Ryax allows for full connectivity of lab equipment with IT ecosystems (even in remote anddistributed contexts). Automatically generate and schedule instructions for all your laboratory equipment!

![lab automation workflow](../_static/lab-automation-use-case.png "lab automation workflow")

Gathering information from a wide range of sparsely connected data sources is a whole challenge in itself, though mandatory in a laboratory context where many systems coexist. The Ryax platform makes it simple to interconnect tablets and portable devices, dedicated instruments such as microscopes, oscilloscopes, various analyzers, specialized operating equipment like ultracentrifuges, diluters, and of course: Lab computers.

Such data pipelines may occur across highly-distributed infrastructural environments where data from remote laboratories needs to be processed locally and then reliably upstreamed to various locations including other labs, customer-specific databases, data lakes, and central clouds. The Ryax platform abstracts these infrastructure layers and ensures robust and predictable data governance over the whole chain.

Efficient use of company resources is the last key pillar of such endeavors. Making sure workforces can decrease time spent on low-value workflows and foster valuable initiatives instead is essential to the sustainability of any lab project. Ryax natively provides a collaborative framework for lab teams to further elaborate lab processes together and benefit from each other's skills and best practices. Companies can build an 'action catalog' in the Ryax Store, whcih is comprised of many built-in (no code needed) actions, as well as internal actions any lab technician can use, reuse, and share to build efficient data processing workflows.
